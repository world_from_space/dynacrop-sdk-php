Getting started
===============

This project is an SDK for `DynaCrop service <http://dynacrop.space>`_ .For more information about the service, please refer to the 
`API documentation <http://docs.dynacrop.space>`_


Installation
-------------

.. code-block:: shell-session

    $ composer require world-from-space/dynacrop

`Issue tracker <https://bitbucket.org/world_from_space/dynacrop-sdk-php/issues>`_
-----------------------------------------------------------------------------------


`Repository <https://bitbucket.org/world_from_space/dynacrop-sdk-php/>`_
-----------------------------------------------------------------------------------
