Examples
===============

.. code-block:: php

    require __DIR__ . '/vendor/autoload.php';

    // Set up authorizations
    DynaCropAuth::set_api_key("YOUR_API_KEY");
    // Polygon coordinates in WTK format 
    $geom = 'POLYGON ((16.73123359680176 49.09308591395033, 16.73123359680176 49.092636274448836, 16.732692718505863 49.092608171844766, 16.7327356338501 49.093647957600226, 16.73123359680176 49.09308591395033))';
    // Create new Polygon using geometry 
    $p = new Polygon($geom, 0.5);
    // Wait till polygon is ready 
    $p->block_till_ready();

    // Create new Rendering Request 
    $rr = new RenderingRequest($p->get_id(), "observation", "2020-01-01", "2020-05-01", "NDVI");
    // Wait till rendering request is ready 
    $rr->block_till_ready();
    // print results 
    print_r($rr->get_result());
    // save colored tiff 
    $rr->save_image_color();
    // save data tiff
    $rr->save_image_raw();
    // tile server url 
    echo $rr->get_tiles();
    // print link to preview app 
    echo $rr->get_demo_url();
