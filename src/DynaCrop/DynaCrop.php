<?php


/**
 * DynaCropAuth
 */
class DynaCropAuth
{
    public static $api_key = NULL;

    /**
     * Return api_key
     *
     * @return string
     */
    public static function get_api_key()
    {
        if (self::$api_key == NULL) {
            throw new Exception('api_key not found! Use DynaCropAuth::set_api_key() to set api key.');
        }
        return self::$api_key;
    }

    /**
     * Set api_key
     *
     * @param  string $api_key
     * @return void
     */
    public static function set_api_key($api_key)
    {
        self::$api_key = $api_key;
    }
}


/**
 * DynaAPIBase
 */
abstract class DynaAPIBase
{
    public $data = NULL;
    public $url = 'https://api-dynacrop.worldfromspace.cz/api/v2/';

    abstract protected function get_data_for_create();
    abstract protected function get_endpoint_name();

    protected function create()
    {

        $url = $this->url . $this->get_endpoint_name() . '/?api_key=' . DynaCropAuth::get_api_key();

        $options = array(
            'http' => array(
                'method'  => 'POST',
                'content' => json_encode($this->get_data_for_create()),
                'header' =>  "Content-Type: application/json\r\n" .  "Accept: application/json\r\n"
            )
        );

        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        $this->data = json_decode($result);
    }

    /**
     * Check if processing has finished
     *
     * @return bool
     */
    public function is_done()
    {
        return $this->get_data()->status == "completed" || $this->get_data()->status == "error";
    }

    /**
     * Check if processing has finished successfully 
     *
     * @return bool
     */
    public function is_completed()
    {
        return $this->get_data()->status == "completed";
    }

    /**
     * Wait until processing finishes 
     *
     * @param  int $num_of_tries
     * @return void
     */
    public function block_till_ready($num_of_tries = 300)
    {
        while (!$this->is_done()) {
            $this->update();
            sleep(1);
            $num_of_tries--;
            if ($num_of_tries <= 0) {
                throw new Exception('Object update timeout');
            }
        }
    }

    /**
     * Get id of object
     *
     * @return int
     */
    public function get_id()
    {
        return $this->get_data()->id;
    }

    /**
     * Get all data 
     *
     * @return object 
     */
    public function get_data()
    {
        if ($this->data) {
            return $this->data;
        }
        throw new Exception('Object has no data');
    }

    /**
     * Refresh object content
     *
     * @return void
     */
    public function update()
    {
        $url = $this->url . $this->get_endpoint_name() . '/' . $this->get_id() . '?api_key=' . DynaCropAuth::get_api_key();
        $options = array(
            'http' => array(
                'method'  => 'GET',
                'header' =>  "Content-Type: application/json\r\n" .
                    "Accept: application/json\r\n"
            )
        );

        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        $this->data = json_decode($result);
    }


    protected function download_file($file_name, $url)
    {
        if (!file_put_contents($file_name, file_get_contents($url))) {
            throw new Exception('File download failed');
        }
    }
}


/**
 * Abstraction over /polygons endpoint 
 */
class Polygon extends DynaAPIBase
{
    public $geometry;
    public $max_mean_cloud_cover;

    /**
     * __construct
     *
     * @param  string $geometry Polygon geometry in WTK format
     * @param  float $max_mean_cloud_cover Max allowed cloud coverage
     * @return void
     */
    function __construct($geometry, $max_mean_cloud_cover)
    {
        $this->geometry = $geometry;
        $this->max_mean_cloud_cover = $max_mean_cloud_cover;
        $this->create();
    }

    protected function get_data_for_create()
    {
        return $data = array(
            'geometry'      => $this->geometry,
            'max_mean_cloud_cover' => $this->max_mean_cloud_cover,
        );
    }

    protected function get_endpoint_name()
    {
        return 'polygons';
    }
}


/**
 * Abstraction over /processing_requests endpoint  
 */
class RenderingRequest extends DynaAPIBase
{
    public $rendering_type;
    public $date_from;
    public $date_to;
    public $layer;
    public $polygon_id;
    
    /**
     * __construct
     *
     * @param  int $polygon_id Define on which polygon should be Rendering Request based on
     * @param  string $rendering_type observation, field_zonation, time_series (see https://docs.dynacrop.space/docs/#/services for avaliable rendering types)
     * @param  string $date_from Specify start of the time range in YYYY-MM-DD format (e.g 2020-12-30)
     * @param  string $date_to Specify end of the time range in YYYY-MM-DD format (e.g 2020-12-30)
     * @param  string $layer NDVI, EVI, etc (seer https://docs.dynacrop.space/docs/#/products for more layers)
     * @return void
     */
    function __construct($polygon_id, $rendering_type, $date_from, $date_to, $layer)
    {
        $this->polygon_id = $polygon_id;
        $this->rendering_type = $rendering_type;
        $this->date_from = $date_from;
        $this->date_to = $date_to;
        $this->layer = $layer;

        $this->create();
    }

    protected function get_data_for_create()
    {
        return $data = array(
            'polygon_id'      => $this->polygon_id,
            'rendering_type' => $this->rendering_type,
            'date_from' => $this->date_from,
            'date_to' => $this->date_to,
            'layer' => $this->layer,
        );
    }

    protected function get_endpoint_name()
    {
        return 'processing_request';
    }

    /**
     * Get results (see https://docs.dynacrop.space/docs/#/services for more details)
     *
     * @return object
     */
    public function get_result()
    {
        if ($this->is_completed()) {
            return $this->get_data()->result;
        }
        throw new Exception('Object has not result');
    }

    /**
     * Save colored tiff to given path 
     *
     * @param  string $path
     * @return void
     */
    public function save_image_color($path = ".")
    {
        $this->download_file($path . "color_" . $this->get_id() . ".tiff", $this->get_result()->color);
    }


    /**
     * Save raw data tiff to given path
     *
     * @param  string $path
     * @return void
     */
    public function save_image_raw($path = ".")
    {
        $this->download_file($path . "raw_" . $this->get_id() . ".tiff", $this->get_result()->raw);
    }

    /**
     * Get url to tile server 
     *
     * @return void
     */
    public function get_tiles()
    {
        return $this->get_result()->tiles_color;
    }

    /**
     * Get url to preview 
     *
     * @return void
     */
    public function get_demo_url()
    {
        return $this->get_result()->tiles_demo;
    }
}
