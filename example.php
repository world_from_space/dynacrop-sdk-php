<?php

require_once('src/DynaCrop/DynaCrop.php');

use DynaCrop\DynaCrop;

DynaCropAuth::set_api_key("api_key");
$geom = 'POLYGON ((16.73123359680176 49.09308591395033, 16.73123359680176 49.092636274448836, 16.732692718505863 49.092608171844766, 16.7327356338501 49.093647957600226, 16.73123359680176 49.09308591395033))';
$p = new Polygon($geom, 0.5);
echo $p->get_data()->id;
$p->block_till_ready();

$rr = new RenderingRequest($p->get_id(), "observation", "2020-01-01", "2020-05-01", "NDVI");
$rr->block_till_ready();
print_r($rr->get_result()); 
$rr->save_image_color("data/");
$rr->save_image_raw();
